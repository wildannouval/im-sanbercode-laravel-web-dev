<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.tambah');
    }

    public function store(Request $request){
        //validasi data
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required|min:5',
        ]);

        //memasukkan request ke database
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        //lempar ke halaman /cast
        return redirect('/cast');
    }

    public function index(){
        //select semua data
        $cast = DB::table('cast')->get();

        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id){
        //mencari data berdasarkan id
        $castid = DB::table('cast')->find($id);

        return view('cast.detail',['castid'=>$castid]);
    }

    public function edit($id){
        //mencari data berdasarkan id
        $castid = DB::table('cast')->find($id);

        return view('cast.edit',['castid'=>$castid]);
    }

    public function update($id, Request $request){
        //validasi data
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required|min:5',
        ]);

        //update data ke database
        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);

        //lempar ke /cast
        return redirect('/cast');
    }

    public function destroy($id){
        //delete data berdasarkan id
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
