@extends('layout.master')
@section('title')
    Halaman Edit Cast
@endsection
@section('content')
@extends('layout.master')
@section('title')
    Halaman Form Cast
@endsection
@section('content')
<form action="/cast/{{$castid->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$castid->nama}}">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" class="form-control" name="umur" value="{{$castid->umur}}">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" rows="10" cols="30" class="form-control">{{$castid->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
@endsection